"""homepage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from base import views as baseView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", baseView.home, name="home"),
    path("about", baseView.about),
    path("project_btc_timeseries", baseView.project_btc_timeseries),
	path("melange", baseView.melange),
	path("hyfrac", baseView.hyfrac),
    path("project_btc_timeseries_dashboard", baseView.project_btc_timeseries_dashboard),
    path("project_btc_timeseries_analysis", baseView.project_btc_timeseries_analysis),
]
