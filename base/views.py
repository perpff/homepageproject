import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio
from plotly.subplots import make_subplots
from plotly.tools import mpl_to_plotly

import numpy as np
import pandas as pd

import base.own_functions.plots as opl

from django.shortcuts import render

import ssl
ssl._create_default_https_context = ssl._create_unverified_context

# Create your views here.
from django.http import HttpResponse

def home(request):
    return (render (request, 'home.html'))

def about(request):
    return (render (request, 'about.html'))

def project_btc_timeseries(request):
    return (render (request, 'project_btc_timeseries.html'))
    
def melange(request):
    return (render (request, 'melange.html'))
   
def hyfrac(request):
    return (render (request, 'hyfrac.html'))

def project_btc_timeseries_dashboard(request):
    chart = opl.create_chart() 
    return (render (request, 'project_btc_timeseries_dashboard.html', {"chart": chart,}))

def project_btc_timeseries_analysis(request):
    return (render (request, 'project_btc_timeseries_analysis.html', {}))