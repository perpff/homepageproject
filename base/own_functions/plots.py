# needed to download ssl-data
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

import os
import pandas as pd

import datetime

import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio
from plotly.subplots import make_subplots
from plotly.tools import mpl_to_plotly

def load_datafile(d):

    urls = {
        'BTCEUR' : "https://www.cryptodatadownload.com/cdd/Binance_BTCEUR_d.csv",
        'LTCEUR' : "https://www.cryptodatadownload.com/cdd/Binance_LTCEUR_d.csv",
        'SOLEUR' : "https://www.cryptodatadownload.com/cdd/Binance_SOLEUR_d.csv",
    }
    
    try:
        url = urls[d]
        
        directory = os.getcwd() + "/static/data/"
        
        file = directory + d + ".pkl"
        
        if os.path.exists(file):
            
            t = datetime.datetime.fromtimestamp(os.path.getmtime(file))
            fileage = (datetime.datetime.today() - t).days
            
            if fileage == 0:
                df = pd.read_pickle(file)
            else:
                df = pd.read_csv(url, delimiter=",", skiprows=[0]) 
                df.to_pickle(file)
        else:
            df = pd.read_csv(url, delimiter=",", skiprows=[0]) 
            df.to_pickle(file)
            
    except:
        print (f"Pattern '{d}' doesn't exist")
        df = pd.DataFrame()

    return df

def create_chart():
    
    df_BTC = load_datafile("BTCEUR")
    df_LTC = load_datafile("LTCEUR")
    df_SOL = load_datafile("LTCEUR")

    df_BTC["OpenDiff"] = df_BTC["Open"].diff()
    df_LTC["OpenDiff"] = df_LTC["Open"].diff()
    df_SOL["OpenDiff"] = df_SOL["Open"].diff()

    fig = make_subplots(cols=1, rows=3, subplot_titles=("BTC-Kurs", "LTC-Kurs", "Solana-Kurs"), shared_xaxes=True, )

    fig.add_trace(go.Scatter(y=df_BTC['Open'], x=df_BTC['Date'], mode='lines',  ), col=1, row=1, )
    fig.add_trace(go.Scatter(y=df_LTC['Open'], x=df_LTC['Date'], mode='lines',  ), col=1, row=2, )
    fig.add_trace(go.Scatter(y=df_SOL['Open'], x=df_SOL['Date'], mode='lines',  ), col=1, row=3, )

    fig.update_yaxes(showline=True, linewidth=1, linecolor='black', mirror=True, showgrid=True, gridwidth=1, gridcolor='blue', title_text="EUR")
    #fig.update_yaxes(col=1, row=1, title_text="EUR")

    fig.update_xaxes(showline=True, linewidth=1, linecolor='black', mirror=True, showgrid=True, gridwidth=1, gridcolor='blue')
    fig.update_xaxes(col=1, row=3, title_text="Date")

    layout = go.Layout(title_text="Exchange rates", paper_bgcolor='rgba(255,255,255,0.8)', plot_bgcolor="rgba(255,255,255,0.0)",
                      # width=1000, height=500,
                    autosize = True, margin={"b":30, "l":30, "r":30, "t":50}, showlegend=False,
                    )
    
    fig.update_layout(layout)
    fig.update_yaxes(automargin=True)
    
    chart = fig.to_html(full_html=True, include_plotlyjs=True, )

    
    return chart